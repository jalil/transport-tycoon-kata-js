import { TransportTycoon, GameState } from './TransportTycoon'

const initialState: GameState = {
  truck1NextAvailability: 0,
  truck2NextAvailability: 0,
  shipNextAvailability: 0,
  lastDeliveryTime: 0
}

describe('TransportTycoon', () => {
  it('delivers A in 5 hours', () => {
    const game = TransportTycoon(initialState, ['A'])
    expect(game.lastDeliveryTime).toEqual(5)
  })
  it('delivers A,B in 5 hours', () => {
    const game = TransportTycoon(initialState, ['A','B'])
    expect(game.lastDeliveryTime).toEqual(5)
  })
  it('delivers B,B in 5 hours', () => {
    const game = TransportTycoon(initialState, ['B','B'])
    expect(game.lastDeliveryTime).toEqual(5)
  })
  it('delivers A,B,B in 7 hours', () => {
    const game = TransportTycoon(initialState, ['A','B','B'])
    expect(game.lastDeliveryTime).toEqual(7)
  })
  it('delivers A,A,B,A,B,B,A,B in 29 hours', () => {
    const game = TransportTycoon(initialState, ['A','A','B','A','B','B','A','B'])
    expect(game.lastDeliveryTime).toEqual(29)
  })
  it('delivers A,B,B,B,A,B,A,A,A,B,B,B in 41 hours', () => {
    const game = TransportTycoon(initialState, ['A','B','B','B','A','B','A','A','A','B','B','B'])
    expect(game.lastDeliveryTime).toEqual(41)
  })
})
