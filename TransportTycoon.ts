export interface GameState {
  truck1NextAvailability: number,
  truck2NextAvailability: number,
  shipNextAvailability: number,
  lastDeliveryTime: number
}

interface VehiculeDeliveryResult {
  deliveredAt: number,
  nextAvailability: number
}

enum VehiculeKind {
  Truck,
  Ship
}

interface Vehicule {
  kind: VehiculeKind
  availability: number
}

interface Truck extends Vehicule{
  kind: VehiculeKind.Truck
  id: number,
}

interface Ship extends Vehicule{
  kind: VehiculeKind.Ship
}

const nextAvailableTruck = (state: GameState): Truck => {
  const id = state.truck1NextAvailability <= state.truck2NextAvailability ? 1 : 2
  return {
    kind: VehiculeKind.Truck,
    id,
    availability: state[`truck${id}NextAvailability`]
  }
}

const moveSegment = (segmentTime: number) => (currentAvailability: number): VehiculeDeliveryResult => ({
  deliveredAt: currentAvailability + segmentTime,
  nextAvailability: currentAvailability + (2 * segmentTime)
})

const fromFactoryToPort = moveSegment(1)
const fromPortToA = moveSegment(4)
const fromFactoryToB = moveSegment(5)

const A = (state: GameState) => {
  const truck = nextAvailableTruck(state)
  const deliveryToPort = fromFactoryToPort(truck.availability)
  const departureFromPort = Math.max(state.shipNextAvailability, deliveryToPort.deliveredAt)
  const deliveryToB = fromPortToA(departureFromPort)
  const lastDeliveryTime = Math.max(state.lastDeliveryTime, deliveryToB.deliveredAt)

  return {
    ...state,
    [`truck${truck.id}NextAvailability`]: deliveryToPort.nextAvailability,
    shipNextAvailability: deliveryToB.nextAvailability,
    lastDeliveryTime
  }
}

const B = (state: GameState) => {
  const truck = nextAvailableTruck(state)
  const deliveryToB = fromFactoryToB(truck.availability)
  const lastDeliveryTime = Math.max(state.lastDeliveryTime, deliveryToB.deliveredAt)

  return {
    ...state,
    [`truck${truck.id}NextAvailability`]: deliveryToB.nextAvailability,
    lastDeliveryTime
  }
}

export const TransportTycoon = (gameState, containers) => {
  const selectRoute = container => container === 'A' ? A : B

  return containers
    .map(selectRoute)
    .reduce((currentState, route) => route(currentState), gameState)
}
